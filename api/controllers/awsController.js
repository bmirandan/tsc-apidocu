var AWS = require('aws-sdk');
var uuid = require('node-uuid');
var fs = require('fs');
var path = require('path');

function updateCredentials(accessKeyId,secretAccessKey){
 AWS.config.update({region: 'us-west-2', credentials: {
  accessKeyId : accessKeyId,
  secretAccessKey :secretAccessKey
 }});
}

exports.createBucket=function(bucketName,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 s3.createBucket({Bucket: bucketName,"ACL":"public-read"}, function(err, data) {
  if (err) {
     cb({status:false,
       error:err});
    }else{
    cb({status:true,
    content:data});
    }
 });
}

exports.createFile=function(bucketName,fileName,content,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});

 

 var params = {Bucket: bucketName, Key: fileName, Body: content,"ACL":"public-read"};
 s3.putObject(params, function(err, data) {
  if (err){
   cb({status:false,
     error:err});
  }
  else{
   cb({status:true,
   content:data});
  }
 });
}
exports.newUploadFile = function(req,res){
 
  return new Promise (function (resolve, reject){
 
  updateCredentials(req.config.credentials.accessKeyId,req.config.credentials.secretAccessKey);
  
  //var base64data = new Buffer(req.data, 'binary');
  
  var s3 = new AWS.S3({ region: 'us-west-2',apiVersion: '2006-03-01'});
  var params={
    "Bucket": req.bucketName,
    "Key": req.config.rootName+req.fileName,
    Body: req.data,
    ContentEncoding: 'base64',
    ContentType: 'image/jpeg',
    ACL: 'public-read',
  }
  params.Body = req.data;

  s3.upload(params,function (err, data) {
    if (err) {
     reject(err);
    } if (data) {
      resolve(data)
    }
  });
 });

}

exports.uploadFile=function(encoded,bucketName,localpath,s3path,fileName,createFileName,config,cb){
  
 updateCredentials(config.credentials.accessKeyId, config.credentials.secretAccessKey);

 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 console.log(fileName);

 var buf = new Buffer(encoded,'base64');
 /*
 var fileStream = fs.createReadStream(fileName);
 console.log(fileStream);
 fileStream.on('error', function(err) {
  cb({status:false,
    error:err});
 });
 */
 // var Meta = {id : "10",nombre:"ejemplo_Metadata1" ,type: "1" };
 var uploadParams = {Bucket: bucketName, Key: '', Body: '',Metadata:{} ,"ACL":"public-read"};
 
 uploadParams.Body = buf;

 if(createFileName==null || createFileName==undefined){
  base = s3path;

  uploadParams.Key =  base.concat(path.basename(fileName));

  console.log(uploadParams.Key);


}else{
  uploadParams.Key = createFileName;
 }
 // call S3 to retrieve upload file to specified bucket
 s3.upload (uploadParams, function (err, data) {
   if (err) {
    cb({status:false,
      error:err});
   } if (data) {
    cb({status:true,
    content:data});
   }
 });
}

exports.getUploadedFile=function(bucketName,fileName,localPath,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 var outFile="";
 if(localPath==null ||localPath==undefined){
  outFile=fileName;
 }else{
  outFile=localPath+"/"+fileName;
 }
 var file = require('fs').createWriteStream(outFile);
 s3.headObject({Bucket: bucketName, Key: fileName}, function (err, metadata) {
  if (err && err.code === 'NotFound') {
   cb({
    status:false,
    error:"File not found"
   });
  } else {
   s3.getObject({Bucket: bucketName, Key: fileName}).createReadStream().pipe(file);
   cb({
    status:true,
    content:"success"
   });
  }
});

}
exports.getObject=function(bucketName,fileName,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 s3.headObject({Bucket: bucketName, Key: fileName}, function (err, metadata) {
  if (err && err.code === 'NotFound') {
   cb({
    status:false,
    error:{error:err,code:err.code}
   });
  }else if(err){
   cb({
    status:false,
    error:{error:err,code:err.code}
   });
  } else {
   s3.getObject({Bucket: bucketName, Key: fileName,"ResponseContentEncoding":"binary"}, function(err, data) {
    if (err) {
     cb({
      status:false,
      error:{error:err,code:err.code}
     });
    }
    else{
     cb({
      status:true,
      content:data
     });
    }
   });
  }
 });

}

exports.getFileMetaData=function(bucketName,fileName,config,cb){

  updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
  var s3 = new AWS.S3({apiVersion: '2006-03-01'});
  var params = {
    Bucket:bucketName, 
    Key: fileName
   };
   s3.headObject(params,(err,data)=>{
//   s3.getSignedUrl('headObject',params,(err,data)=>{
    if(err){
    console.log(err);
       } 
    else{

      console.log(data);
    }
   });
   }

exports.getFileData=function(bucketName,fileName,localPath,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 var outFile="";
 if(localPath==null ||localPath==undefined){
  outFile=fileName;
 }else{
  outFile=localPath+"/"+fileName;
 }
 var file = require('fs').createWriteStream(outFile);

 s3.getObject({Bucket: bucketName, Key: fileName}).createReadStream().pipe(file);
 cb({
  status:true,
  content:"success"
 });
}

exports.listAllBucket=function(config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 s3.listBuckets(function(err, data) {
    if (err) {
     cb({status:false,
       error:err});
    } else {
     cb({
      status:true,
      content:data.Buckets
     });
    }
 });
}

exports.listAllFiles=function(bucketName,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 var allKeys = [];
 s3.listObjects({Bucket: bucketName, Prefix:config.credentials.rootName}, function(err, data){
  if(err){
   cb({status:false,
     error:err});
  }else{
   data.Contents.forEach(function(data){
    allKeys.push(data.Key);
    // allKeys.push(data);
   });
   cb({
    status:true,
    content:allKeys
   });
  }
 });
}

exports.removeFile=function(bucketName,fileName,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 s3.deleteObject({Bucket:bucketName,Key:fileName}, function(err, data) {
   console.log(fileName);
   if (err) {
    cb({status:false,
      error:err});
   }
   else{
    cb({
     status:true,
     content:data
    });
   }
 });
}

exports.removeBucket=function(bucketName,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 s3.deleteBucket({Bucket:bucketName},function(err, data) {
    if (err) {
     cb({status:false,
       error:err});
    } else {
     cb({
      status:true,
      content:data
     });
    }
 });
}


exports.getBucketPermission=function(bucketName,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 s3.getBucketAcl({Bucket:bucketName}, function(err, data) {
   if (err) {
    cb({status:false,
      error:err});
   } else {
    cb({
     status:true,
     content:data.Grants
    });
   }
});
}

exports.isBucketExist=function(bucketName,config,cb){
 updateCredentials(config.credentials.accessKeyId,config.credentials.secretAccessKey);
 var s3 = new AWS.S3({apiVersion: '2006-03-01'});
 var bucketExist=0;
 s3.listBuckets(function(err, data) {
    if (err) {
     cb({status:false,
       error:err});
    } else {
     data.Buckets.forEach(function(bucketData){
      if(bucketData.Name.toLowerCase()==bucketName.toLowerCase()){
       bucketExist=1;
      }
     });
     if(bucketExist==1){
      cb({
       status:true,
       isExist:true,
       content:data.Buckets
      });
     }else{
      cb({
       status:true,
       isExist:false,
       content:data.Buckets
      });
     }
    }
 });
}
