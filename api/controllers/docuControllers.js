'use strict';
var fs = require('fs');
var mongoCtrl = require("./mongoDbController.js");
var s3BucketMgt=require("./awsController.js");
var config=require("../../config.js");
var bucketName = config.credentials.bucketName;
var fileName="";
var fileContents="Demo grupo 3."
var fileToUpload = "";

// s3BucketMgt.createBucket(bucketName,config,function(data){
//  if(data.status==false){
//   console.log("Error in Bucket Creation:"+data.error);
//  }else{
//   console.log("Bucket Created");
//  }
// });
//
// s3BucketMgt.createFile(bucketName,fileName,fileContents,config,function(data){
//  if(data.status==false){
//   console.log("Error in File Creation:"+data.error);
//  }else{
//   console.log("File Created in Bucket.");
//  }
// });

// s3BucketMgt.uploadFile(bucketName,fileToUpload,null,config,function(data){
//  if(data.status==false){
//   console.log("Error in Upload file:"+data.error);
//  }else{
//   console.log("File Uploaded in Bucket.");
//  }
// });

// s3BucketMgt.getUploadedFile(bucketName,"logo.jpg",null,config,function(data){
//  if(data.status==false){
//   console.log("Error in getting uploaded File:"+data.error);
//  }else{
//   console.log("File created on Local.");
//  }
// });

// s3BucketMgt.getObject(bucketName,"logo1.jpg",config,function(data){
//  if(data.status==false){
//   console.log("Error in getting uploaded File:"+data.error.code);
//  }else{
//   console.log("Content of File:"+data.content.Body);
//  }
// });

// s3BucketMgt.getFileData(bucketName,fileName,null,config,function(data){
//  if(data.status==false){
//   console.log("Error in getting File data:"+data.error);
//  }else{
//   console.log("File created on Local.");
//  }
// });

// s3BucketMgt.listAllBucket(config,function(data){
//  if(data.status==false){
//   console.log("Error in getting list of Buckets:"+data.error);
//  }else{
//   console.log("Buckets:"+JSON.stringify(data.content));
//  }
// });

// s3BucketMgt.listAllFiles(bucketName,config,function(data){
//  if(data.status==false){
//   console.log("Error in getting list of Files:"+data.error);
//  }else{
//   console.log("Files In Bucket:"+(data.content));
//  }
// });

// s3BucketMgt.removeBucket(bucketName,config,function(data){
//  if(data.status==false){
//   console.log("Error in Remove Bucket:"+data.error);
//  }else{
//   console.log("Bucket is removed:"+bucketName);
//  }
// });

// s3BucketMgt.getBucketPermission(bucketName,config,function(data){
//  if(data.status==false){
//   console.log("Error in getting list of Buckets:"+data.error);
//  }else{
//   console.log("Buckets:"+JSON.stringify(data.content));
//  }
// });

// s3BucketMgt.isBucketExist(bucketName,config,function(data){
//  if(data.status==false){
//   console.log("error in isBucketExist:"+data.error);
//  }else{
//   console.log("Bucket exist:"+data.isExist);
//  }
// });


exports.list_all_folders = function(req, res){
  
  

  mongoCtrl.listFolders()
  .then((rep)=>{
    res.json(rep);
  })
  .catch((err)=>{
    res.json(err);
  })    
};

exports.create_a_folder = function(req, res){
  var new_folder = new FolderBD(req.body);
  new_folder.save(function(err, folder){
    if(err)
      res.send(err);
    res.json(folder);
  });
};

exports.read_a_folder = function(req, res){
 
mongoCtrl.findbyIdInFolders(req)
.then((rep)=>{
  console.log("Folder found");
  res.json(rep);
})
.catch((err)=>{
  console.log("Folder not found Error: "+ err);
});
};
//Funcion Muestra Archivos AWS
exports.list_all_files =function(req, res){
/*
  var todelete="grupo3/penta.png";
  s3BucketMgt.removeFile(bucketName, todelete ,config,function(data){
    if(data.status==false){
     console.log("Error in Remove file:"+data.error);
    }else{
    }
  });
*/
  mongoCtrl.listDocuments()
  .then((rep)=>{
    s3BucketMgt.listAllFiles(bucketName,config,function(data){
      if(data.status==false){
       console.log("Error in getting list of Files:"+data.error);
       }else{
        console.log("AWS S3 il258 GRUPO Files");
        console.log(data.content);
        res.json(rep); 
       }
      });
        
  })
  .catch((err)=>{
    res.json(err);
  })

  
};

//Funcion Subir un Archivo AWS

//file path requerido
exports.upload_a_file = function(req,res){
  var ts_hms= Date.now();
  //se debe extraer el path directo de father id
  var s3path=""
  var encrypted = req.query.upfile;
  var localpath = req.query.upfile;
  
  var document = {
   name: req.query.name,
   parent_folder_id: req.query.id_parent_folder,
   full_path: config.credentials.rootName + s3path,
   created_at:  ts_hms,
   created_by: "elbenja",
   updated_at: ts_hms,
   updated_by: "el benja"
  }  
  

 
  //var bitmap = fs.readFileSync(req.query.upfile);
  //var buf = new Buffer(bitmap,'base64');
  //var encoded = buf.toString('base64');
  var encoded = req.query.upfile;
  // console.log("Iniciop ::  "+ encoded + " FIN ");

  // convert binary data to base64 encoded string
    
   s3BucketMgt.uploadFile(encoded,bucketName,localpath,document.full_path,localpath,null,config,function(data){
      if(data.status==false){
       console.log("Error in Upload file:"+data.error);
      }else{
        mongoCtrl.insertFileToDb(document).then(function(data){
          res.json(data.ok);
        });
       console.log("File Uploaded in Bucket.");
      }
     });
    }
//});
//}

exports.upload_new_version_file = function(req,res){
  
  };
  


exports.write_a_folder = function(req,res){
  
  //Busqueda de padre por id para fullpath en bd
if(req.query.id_parent_folder==-1){
  var full_path= config.credentials.rootName;
}
else{
/*
request = {id: req.query.id_parent_folder}
  mongoCtrl.findbyIdInFolders(request)
  .then((res)=>{    
  //add to contents en carpeta padre  

  })
  .catch((err)=>{

  })
*/
  //function return folder papa ( debe ser recursivo hasta que el nodo sea hijo de -1)
}
  var ts_hms = Date.now(); 
  var folderToUpload = {
    
    name: req.query.name,
    parent_folder_id: req.query.id_parent_folder,
    full_path: full_path,
    created_at:  ts_hms,
    created_by: "elbenja",
    updated_at: ts_hms,
    updated_by: "el benja",
    contents: []
    
  }   
  mongoCtrl.insertFolderToDb(folderToUpload)
  .then((resp)=>{
    res.json(resp);
  })
  .catch((err)=>{
    res.json(err);
  })  
};

exports.update_a_folder = function(req,res){
  //con un id se debe updatear el folder 
  mongoCtrl.modifybyIDinFolders(req)
  .then((resp)=>{
    res.json(resp);
  })
  .catch((err)=>{
    res.json(err);s
  })
}

exports.update_a_file = function(req,res){
  //con un id se debe updatear el folder 
  
  var id = req.params.folderId;
};


exports.updata_meta_file = function(req,res){
  //con un id se debe updatear el folder 
  var id = req.params.folderId;
};

//Leer un Archivo
exports.read_a_file =function(req, res){


  mongoCtrl.findbyIdInDocuments(req)
  .then((rep)=>{
    res.json(rep)
  })
  .catch((err)=>{
    console.log("Error getting file from data base");
  })

};


//Eliminar un archivo
exports.delete_a_file = function(req,res){
    mongoCtrl.findbyIdInDocuments(req)
    .then((res)=>{

      var todelete = res.full_path + res.name;
      
      s3BucketMgt.removeFile(bucketName, todelete ,config,function(data){
        if(data.status==false){
         console.log("Error in Remove file:"+data.error);
        }
        else{
        mongoCtrl.deleteFile(req)
        .then((rep)=>{
          res.status(201).send("File removed from database")
          res.json(rep);
        })
        .catch((err)=>{
          err.status(500).send('Error delete operation mongo DB!');
          err.json(err);
         });
          console.log("File removed from Bucket:"+bucketName);
        }
      });
    })
    .catch((err)=>{
      console.log("can't find file to delete in database")
      res.json(err);
    });
    
  

};

 exports.delete_a_folder = function(req,res){
  /*Deletes a single folder based on the ID supplied, 
  only if folder is empty if the folder is NOT empty, 
  the user must provide the param FORCE, 
  to recursively delete ALL its contents. */

  mongoCtrl.findbyIdInFolders(req)
  .then((resp)=>{
  if(true){
    //buscar contents y borrar todos 
     mongoCtrl.deleteFolder(req)
    .then((resp)=>{
      console.log("Delete operation complete: " + resp);
      res.json(resp.ok);
     })
    .catch((err)=>{
      console.log("Delete opertation error:" + err);
      res.json(err);
    });
    
  }
  else{
    var err = "Folder not empty";
    res.json(err);
    console.log("Can't delete this")
  }    

  })
  .catch((err)=>{
    res.json(err);
    console.log("Folder id not found error: " + err);
  })
 };
 