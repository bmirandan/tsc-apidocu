var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var config = require('../../config.js')



var uri = config.mongoconfig.uri;
var dbname = "api"

exports.insertFolderToDb = function(req, res){
  
return new Promise (function (resolve, reject){
  MongoClient.connect(uri)
  .then((db) =>{
    var dbase = db.db(dbname);
    console.log("Successfully conected to server");
    console.log(req);
    dbase.collection('folders').insertOne(
      {"name": req.name,
       "parent_folder_id": req.parent_folder_id,
       "full_path": req.full_path,
       "created_at": req.created_at,
       "created_by": req.created_by,
       "updated_at": req.updated_at,
       "updated_by": req.updated_by,
       "contents": req.contents })
    .then((res) =>{
      console.log("insertion accomplish");

      resolve(res);
      db.close();
    }).catch((err)=>{
      console.log("failed insertion");
    })   

  }).catch((error)=>{
    assert.equal(null, error);
    console.log("Can't connect to mongo cloud db");
    reject();
  })
});
};

exports.connectDb = function(req, res){
  return new Promise (function (resolve, reject){
    MongoClient.connect(uri)
    .then((db) =>{
      resolve(db);
    })
    .catch((err)=>{
      reject(err);
    });
  });
};
exports.modifybyIDinFolders = function(req,res){
  
  return new Promise (function (resolve,reject){
    MongoClient.connect(uri)
    .then((db)=>{
      var dbase = db.db(dbname);
      var params = req.query;
      dbase.collection('folders').updateOne(
                {"_id":req.folderId},
                {$set: req.query}
      ).then((res)=>{
        resolve(res);
        db.close();
        
      })
      .catch((err)=>{
        console.log("Can't update element");
        reject(err);
      })
     
     
    })
    .catch((err)=>{
    console.log("Can't connect to mongo cloud db"); 
    reject(err);
    })
  })
}

exports.findbyIdInDocuments = function(req, res){
  return new Promise (function (resolve, reject){
    MongoClient.connect(uri)
    .then((db) =>{
      var dbase = db.db(dbname);
      
      dbase.collection('documents').findOne({ id: req.id})
      .then((res) => {
        console.log("Search complete");
        resolve(res);
        db.close();
      }).catch((err)=>{
        console.log("Search failed");
        reject(err);   
      }) 
    })
    .catch((err)=>{
      reject(err);
    });
  });
};


exports.findbyIdInFolders = function(req, res){
  return new Promise (function (resolve, reject){
    MongoClient.connect(uri)
    .then((db) =>{
      var dbase = db.db(dbname);
      
      dbase.collection('folders').findOne({ id: req.id})
      .then((res) => {
        console.log("Search complete");
        resolve(res);
        db.close();
      }).catch((err)=>{
        console.log("Search failed");
        reject(err);   
      }) 
    })
    .catch((err)=>{
      reject(err);
    });
  });
};
exports.insertFileToDb = function(req, res){
  
return new Promise (function (resolve, reject){
  MongoClient.connect(uri)
  .then((db) =>{
    var dbase = db.db(dbname);
    console.log("successfully conected to server");
    dbase.collection('documents').insertOne({
      "access_url": "http://il258.s3.us-west-2.amazonaws.com/"+req.full_path+req.name,
      "name": req.name,
      "parent_folder_id": req.parent_folder_id,
      "full_path": req.full_path,
      "created_at": req.created_at,
      "created_by": req.created_by,
      "updated_at": req.updated_at,
      "updated_by": req.updated_by
    })
    .then((res) =>{
      console.log("accept insertion ");
      resolve(res);
      db.close();
    }).catch((err)=>{
      console.log("failed insertion");
    })   

  }).catch((error)=>{
    assert.equal(null, error);
    console.log("Can't connect to mongo cloud db");
    reject();
  })
});
}
exports.deleteFolder = function(req,res){
    
    return new Promise (function (resolve, reject){
      MongoClient.connect(uri)
      .then((db) =>{
        var dbase = db.db(dbname);
        dbase.collection('folders').deleteOne({id: req.id})
        .then((data)=>{
          console.log("Folder id:"+req.params.folderId + " delete successful!");
          resolve(data);
          db.close();          
        }).catch((err)=>{
          reject(err);
        })
      })
      .catch((err)=>{
        reject(err);
      });
    });
  };
  
  exports.deleteFile = function(req,res){
    
        return new Promise (function (resolve, reject){
          MongoClient.connect(uri)
          .then((db) =>{
            var dbase = db.db(dbname);
            dbase.collection('documents').deleteOne({id: req.id})
            .then((data)=>{
              console.log("File id:" + req.id + " delete successful!");
              resolve(data);
              db.close();              
            }).catch((err)=>{
              reject(err);
            })
          })
          .catch((err)=>{
            reject(err);
          });
        });
      };

exports.listFolders = function(req, res){
  return new Promise (function (resolve, reject){
    MongoClient.connect(uri)
    .then((db) =>{
      var dbase= db.db(dbname);

      dbase.collection("folders").find({}).toArray(function(err, result) {
        if (err) throw err;
       resolve(result);        
       db.close();
       
       
         });
     
    })
    .catch((err)=>{
      reject(err);
    });
  });
};

exports.listDocuments= function(req, res){
  return new Promise (function (resolve, reject){
    MongoClient.connect(uri)
    .then((db) =>{
      var dbase= db.db(dbname);
      // RETORNAR SOLO LA PARTE DE LOS REQ
      dbase.collection("documents").find({}).toArray(function(err, result) {
        if (err) throw err;
       resolve(result);           
       db.close();
       
     });
    })
    .catch((err)=>{
      reject(err);
    });
  });
};


           /* 
           
    return new Promise(function (fulfill, reject){
     collectionname = "folders"
      MongoClient.connect(uri, function(err, db) {
        assert.equal(null, err);
        var dbase = db.db("mydb");
        dbase.collection("folders").find({}).toArray(function(err, result) {
          if (err) throw err;
         console.log(result)
          

         
           });
            dbase.collections().then(function(res){
             console.log(res);
             
          ,   
            function(err,result){
             assert.equal(err,null);
             assert.equal(1,result.result.n);
             assert.equal(1, result.ops.length);
            
           });
                db.close();
     
       if (err) reject(err);      
        else fulfill( res={collectionname:collectionname});
    
     */

 