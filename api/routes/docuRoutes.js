'use strict';
module.exports = function(app){
	var docuCtrl =require('../controllers/docuControllers');

	// rutas de carpeta
	app.route('/folders')
		.get(docuCtrl.list_all_folders)
		.post(docuCtrl.write_a_folder)
	
	app.route('/folders/:folderId')
		.get(docuCtrl.read_a_folder)
		.patch(docuCtrl.update_a_folder)
		.delete(docuCtrl.delete_a_folder);

	app.route('/files')
		.get(docuCtrl.list_all_files)
		.post(docuCtrl.upload_a_file);
		
	app.route('/files/:filesId')
		.get(docuCtrl.read_a_file)
		.put(docuCtrl.upload_new_version_file)
		.patch(docuCtrl.updata_meta_file)
		.delete(docuCtrl.delete_a_file);
		
};